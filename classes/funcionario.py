#from ata import Ata
from participantereuniao import ParticipanteReuniao


class Funcionario:
    def __init__(self):
        self.nome = ""
        self.matricula = 0
        self.nascimento = ""
        self.email = ""
        self.partr = ParticipanteReuniao()
        self.list_incluir = []
        super().__init__()

    def incluir(self):
        self.nome = str(input("Informe o nome: "))
        self.list_incluir.append(self.nome)
        self.matricula = int(input("Informe a matricula: "))
        self.list_incluir.append(self.matricula)
        self.nascimento = str(input("Informe a data de nascimento: "))
        self.list_incluir.append(self.nascimento)
        self.email = str(input("Informe o email: "))
        self.list_incluir.append(self.email)
        return self.list_incluir

    def selecionar(self):
        self.partr.selecionarParticipante(self.list_incluir)
        
