from funcionario import Funcionario


class Ata:

    def __init__(self):
        self.tituloReuniao = ""
        self.dataEmissao = ""
        self.participantesFuncionarios = ""
        self.inicio = ""
        self.termino = ""
        self.pauta= ""
        self.descricao= ""
        self.palavrasChaves = ""
        self.tipo = ""
        self.status = ""
        self.ata_detail = []
        self.func = Funcionario()

    def emitirAta(self):
        self.tituloReuniao = str(input("Informe o título da reunião: "))
        self.ata_detail.append(self.tituloReuniao)
        self.dataEmissao = str(input("Informe a data de emissão: "))
        self.ata_detail.append(self.dataEmissao)
        self.participantesFuncionarios = str(input("Informe o participante: "))
        self.ata_detail.append(self.participantesFuncionarios)
        self.inicio = str(input("Informa o horário de início: "))
        self.ata_detail.append(self.inicio)
        self.termino = str(input("Informe o horário de término: "))
        self.ata_detail.append(self.termino)
        self.pauta = str(input("Informe a pauta: "))
        self.ata_detail.append(self.pauta)
        self.descricao = str(input("Descreva a pauta: "))
        self.ata_detail.append(self.descricao)
        self.palavrasChaves = str(input("Informe a palavra chave da reunião: "))
        self.ata_detail.append(self.palavrasChaves)
        self.termino = str(input("Informe o tipo: "))
        self.ata_detail.append(self.termino)
        self.status = str(input("Informe o estatus da reunião: "))
        self.ata_detail.append(self.status)
        return self.ata_detail

    def exibirAta(self):
        for i in self.ata_detail:
            print(i)

    def salvarAta(self):
        self.func.salvarAta()        

    def atualizarAta(self):
        self.exibirAta()
