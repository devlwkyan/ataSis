class Sugestao:

    def __init__(self):
        self.data = ""
        self.descricao = ""
        self.sugg = []

    def emitirSugestao(self):
        self.data = str(input("Informe a data: "))
        self.descricao = str(input("Descreva a sugestão: "))
        self.sugg.append(self.data + " | " + self.descricao )

    def selecionarSugestao(self):
        '''
        Mover esse pedaço de código para função main:
        >>> DAQUI <<<
        ''' 
        print("Escolha a sugestão: ")
        count = 0
        for i in self.sugg:
            count += 1
            print(f"""
            # Sugestão número: {count}
            -------------------------------------------
            |DATA       | SUGESTÃO
            -------------------------------------------
            |{i}
            |__________________________________________
            """)
        # >> ATÉ AQUI <<<
        #sel_sugest = int(input("informe o número referente a"))


