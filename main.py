from sys import path
path.append('classes')
from classes import sugestao
from classes import participantereuniao
from classes import participanteexterno
from classes import funcionario
from classes import ata

def main():
    op = int(input("""
    BEM VINDO AO SISTEMA DE ATAS, ESCOLHA UMA DAS OPÇÕES ABAIXO:
    -----------------------------------------------------------
    [0] - Emitir ou Selecionar Sugestão
    [1] - Incluir ou Selecionar Funcionario
    [2] - Incluir ou Selecionar Participante Externo
    [3] - Incluir Pessoa na Reunião
    [4] - Sobre Ata
    Opção: """))

    if op == 0:
        x = str(input("Digite E para emitir sugestão, ou S para selecionar sugestão: "))
        if x.upper() == "E":
            sugestao.Sugestao().emitirSugestao()
            main()
        elif x.upper() == "S":
            sugestao.Sugestao().selecionarSugestao()
            main()
        else:
            print("Opção invalida!")
            main()
    elif op == 1:
        x = str(input("Digite I para inserir funcionário, ou S para selecionar funcionário: "))
        if x.upper() == "I":
            funcionario.Funcionario().incluir()
            main()
        elif x.upper() == "S":
            funcionario.Funcionario().selecionar()
            main()
        else:
            print("Opção invalida, tente novamente!")
            main()
    elif op == 2:
        x = str(input("Digite I para inserir participante externo, ou S para selecionar participante externo: "))
        if x.upper() == "I":
            participanteexterno.ParticipanteExterno().incluir()
            main() 
        elif x.upper() == "S":
            participanteexterno.ParticipanteExterno().selecionar()
            main()
        else:
            print("Opção inválida, tente novamente!")
            main() 
    elif op == 3:
        x = int(input("Se a pessoa a ser incluida na reunião for funcionário, digite 0, ou se a pessoa for participante externo, digite 2: "))
        if x == 0:
            participantereuniao.ParticipanteReuniao().incluirParticipante(funcionario.Funcionario().incluir())
            main()
        elif x == 1:
            participantereuniao.ParticipanteReuniao().incluirParticipante(participanteexterno.ParticipanteExterno().incluir())
            main()
        else:
            print("opção invalida, tente novamente")
            main()
    else:
        print("Obrigado por usar nosso sistema de atas")

main()
